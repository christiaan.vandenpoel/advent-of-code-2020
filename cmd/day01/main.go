package main

import (
	"fmt"

	"advent-of-code-2020/pkg/day01"
	"advent-of-code-2020/pkg/utils"
)

func main() {
	input := utils.ReadIntLinesFromFile("input.txt")

	fmt.Printf("ReportRepairForTwoItems:         %d\n", day01.ReportRepairForTwoItems(input))
	fmt.Printf("ReportRepairForThreeItems:       %d\n", day01.ReportRepairForThreeItems(input))
}
