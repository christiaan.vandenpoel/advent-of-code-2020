package main

import (
	"fmt"

	"advent-of-code-2020/pkg/day02"
	"advent-of-code-2020/pkg/utils"
)

func main() {
	input := utils.ReadStringFromFile("input.txt")

	fmt.Printf("CountValidPasswords:           %d\n", day02.CountValidPasswords(input))
	fmt.Printf("CountValidPasswordsSecondPart: %d\n", day02.CountValidPasswordsSecondPart(input))
}
