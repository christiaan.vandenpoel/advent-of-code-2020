package main

import (
	"fmt"

	"advent-of-code-2020/pkg/day03"
	"advent-of-code-2020/pkg/utils"
)

func main() {
	input := utils.ReadStringFromFile("input.txt")

	fmt.Printf("TobogganTrajectorySingleSlope:    	%d\n", day03.TobogganTrajectorySingleSlope(input))
	fmt.Printf("TobogganTrajectoryMultipleSlopes:   %d\n", day03.TobogganTrajectoryMultipleSlopes(input))
}
