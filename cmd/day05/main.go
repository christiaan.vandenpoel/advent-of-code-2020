package main

import (
	"fmt"

	"advent-of-code-2020/pkg/day05"
	"advent-of-code-2020/pkg/utils"
)

func main() {
	input := utils.ReadStringFromFile("input.txt")

	fmt.Printf("BinaryBoardingHighestSeatId: %d\n", day05.BinaryBoardingHighestSeatId(input))
	fmt.Printf("BinaryBoardingFindMySeatId:  %d\n", day05.BinaryBoardingFindMySeatId(input))
}
