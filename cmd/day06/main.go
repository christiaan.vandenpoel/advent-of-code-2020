package main

import (
	"fmt"

	"advent-of-code-2020/pkg/day06"
	"advent-of-code-2020/pkg/utils"
)

func main() {
	input := utils.ReadStringFromFile("input.txt")

	fmt.Printf("CustomCustomsAnswerTotal:			%d\n", day06.CustomCustomsAnyAnswerTotal(input))
	fmt.Printf("CustomCustomsEveryoneAnsweredTotal: %d\n", day06.CustomCustomsEveryoneAnsweredTotal(input))
}
