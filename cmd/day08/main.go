package main

import (
	"fmt"

	"advent-of-code-2020/pkg/day08"
	"advent-of-code-2020/pkg/utils"
)

func main() {
	input := utils.ReadStringFromFile("input.txt")

	fmt.Printf("HandheldHaltingAccumulateUniqueOps: %d\n", day08.HandheldHaltingAccumulateUniqueOps(input))
	fmt.Printf("HandheldHaltingRunUntilEnd: %d\n", day08.HandheldHaltingRunUntilEnd(input))
}
