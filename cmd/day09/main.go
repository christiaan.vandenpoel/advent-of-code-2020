package main

import (
	"fmt"

	"advent-of-code-2020/pkg/day09"
	"advent-of-code-2020/pkg/utils"
)

func main() {
	input := utils.ReadStringFromFile("input.txt")

	fmt.Printf("EncodingErrorFirstInvalidWithPreamble: %d\n", day09.EncodingErrorFirstInvalidWithPreamble(input, 25))
	fmt.Printf("EncodingErrorEncryptionWeakness: %d\n", day09.EncodingErrorEncryptionWeakness(input, 25))
}
