package main

import (
	"fmt"

	"advent-of-code-2020/pkg/day10"
	"advent-of-code-2020/pkg/utils"
)

func main() {
	input := utils.ReadStringFromFile("input.txt")

	fmt.Printf("AdapterArrayJoltDifference:       %d\n", day10.AdapterArrayJoltDifference(input))
	fmt.Printf("AdapterArrayPossibleCombinations: %d\n", day10.AdapterArrayPossibleCombinations(input))
}
