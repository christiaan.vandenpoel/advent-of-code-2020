package main

import (
	"fmt"

	"advent-of-code-2020/pkg/day11"
	"advent-of-code-2020/pkg/utils"
)

func main() {
	input := utils.ReadStringFromFile("input.txt")

	fmt.Printf("SeatingSystemTotalOccupiedSeatsDirectlyAdjacent: %d\n", day11.SeatingSystemTotalOccupiedSeatsDirectlyAdjacent(input))
	fmt.Printf("SeatingSystemTotalOccupiedSeatsVisiblyAdjacent:  %d\n", day11.SeatingSystemTotalOccupiedSeatsVisiblyAdjacent(input))
}
