package main

import (
	"fmt"

	"advent-of-code-2020/pkg/day12"
	"advent-of-code-2020/pkg/utils"
)

func main() {
	input := utils.ReadStringFromFile("input.txt")

	fmt.Printf("RainRiskManhattanDistance:             %d\n", day12.RainRiskManhattanDistance(input))
	fmt.Printf("RainRiskManhattanDistanceWithWaypoint: %d\n", day12.RainRiskManhattanDistanceWithWaypoint(input))
}
