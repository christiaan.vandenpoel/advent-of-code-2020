package main

import (
	"fmt"

	"advent-of-code-2020/pkg/day13"
	"advent-of-code-2020/pkg/utils"
)

func main() {
	input := utils.ReadStringFromFile("input.txt")

	fmt.Printf("ShuttleSearchFirstAvailableBus:                          %d\n", day13.ShuttleSearchFirstAvailableBus(input))
	fmt.Printf("ShuttleSearchFirstTimestampWithSameDepartureDifferences: %d\n", day13.ShuttleSearchFirstTimestampWithSameDepartureDifferences(input))
}
