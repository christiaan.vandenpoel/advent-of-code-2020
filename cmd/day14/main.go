package main

import (
	"fmt"

	"advent-of-code-2020/pkg/day14"
	"advent-of-code-2020/pkg/utils"
)

func main() {
	input := utils.ReadStringFromFile("input.txt")

	fmt.Printf("DockingDataMemorySum:         %d\n", day14.DockingDataMemorySum(input))
	fmt.Printf("DockingDataMemorySumVersion2: %d\n", day14.DockingDataMemorySumVersion2(input))
}
