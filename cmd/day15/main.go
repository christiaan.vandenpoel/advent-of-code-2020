package main

import (
	"fmt"

	"advent-of-code-2020/pkg/day15"
	"advent-of-code-2020/pkg/utils"
)

func main() {
	input := utils.ReadStringFromFile("input.txt")

	fmt.Printf("RambunctiousRecitation2020thNumber: %d\n", day15.RambunctiousRecitationNthNumber(input, 2020))
	fmt.Printf("RambunctiousRecitationLargethNumber: %d\n", day15.RambunctiousRecitationNthNumber(input, 30000000))
}
