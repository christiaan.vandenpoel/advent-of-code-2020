package main

import (
	"fmt"

	"advent-of-code-2020/pkg/day16"
	"advent-of-code-2020/pkg/utils"
)

func main() {
	input := utils.ReadStringFromFile("input.txt")

	fmt.Printf("TicketTranslationInvalidTickets: %d\n", day16.TicketTranslationInvalidTickets(input))
	fmt.Printf("Y: %d\n", day16.Y(input))
}
