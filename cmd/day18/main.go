package main

import (
	"fmt"

	"advent-of-code-2020/pkg/day18"
	"advent-of-code-2020/pkg/utils"
)

func main() {
	input := utils.ReadStringFromFile("input.txt")

	fmt.Printf("OperationOrderSumOfLineResults: %d\n", day18.OperationOrderSumOfLineResults(input))
	fmt.Printf("Y: %d\n", day18.Y(input))
}
