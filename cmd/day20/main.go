package main

import (
	"fmt"

	"advent-of-code-2020/pkg/day20"
	"advent-of-code-2020/pkg/utils"
)

func main() {
	input := utils.ReadStringFromFile("input.txt")

	fmt.Printf("X: %d\n", day20.X(input))
	fmt.Printf("Y: %d\n", day20.Y(input))
}
