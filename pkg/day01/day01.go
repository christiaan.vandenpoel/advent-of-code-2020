package day01

func ReportRepairForTwoItems(report []int) int {
	lines := len(report)
	for index := 0; index < lines; index++ {
		for next := index + 1; next < lines; next++ {
			if report[index]+report[next] == 2020 {
				return report[index] * report[next]
			}
		}
	}
	return 0
}

func ReportRepairForThreeItems(report []int) int {
	lines := len(report)
	for index := 0; index < lines; index++ {
		for next := index + 1; next < lines; next++ {
			for last := next + 1; last < lines; last++ {
				if report[index]+report[next]+report[last] == 2020 {
					return report[index] * report[next] * report[last]
				}
			}
		}
	}
	return 0
}
