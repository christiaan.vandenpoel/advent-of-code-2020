package day01_test

import (
	"testing"

	"advent-of-code-2020/pkg/day01"
)

func TestReportRepairForTwoItems(t *testing.T) {
	tests := []struct {
		in  []int
		out int
	}{
		{[]int{2020, 0}, 0},
		{[]int{2019, 1}, 2019},
		{[]int{1721, 979, 366, 299, 675, 1456}, 514579},
	}

	for _, test := range tests {
		actual := day01.ReportRepairForTwoItems(test.in)
		if actual != test.out {
			t.Errorf("ReportRepairForTwoItems(%d) => %d, want %d", test.in, actual, test.out)
		}
	}
}

func TestReportRepairForThreeItems(t *testing.T) {
	tests := []struct {
		in  []int
		out int
	}{
		{[]int{1721, 979, 366, 299, 675, 1456}, 241861950},
	}

	for _, test := range tests {
		actual := day01.ReportRepairForThreeItems(test.in)
		if actual != test.out {
			t.Errorf("ReportRepairForThreeItems(%d) => %d, want %d", test.in, actual, test.out)
		}
	}
}
