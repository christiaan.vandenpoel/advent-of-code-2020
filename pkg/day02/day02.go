package day02

import (
	"advent-of-code-2020/pkg/utils"
	"regexp"
	"strings"
)

func CountValidPasswords(input string) int {
	validPasswordCount := 0

	lines := strings.Split(input, "\n")

	regex := regexp.MustCompile(`(?P<min>\d+)-(?P<max>\d+) (?P<char>\w): (?P<password>\w+)`)

	for _, line := range lines {
		matches := regex.FindStringSubmatch(line)

		min := utils.MustInt(matches[1])
		max := utils.MustInt(matches[2])
		character := matches[3]
		password := matches[4]

		passwordMap := make(map[string]int)
		for _, char := range password {
			passwordMap[string(char)]++
		}
		if passwordMap[character] >= min && passwordMap[character] <= max {
			validPasswordCount++
		}
	}

	return validPasswordCount
}

func CountValidPasswordsSecondPart(input string) int {
	validPasswordCount := 0

	lines := strings.Split(input, "\n")

	regex := regexp.MustCompile(`(?P<min>\d+)-(?P<max>\d+) (?P<char>\w): (?P<password>\w+)`)

	for _, line := range lines {
		matches := regex.FindStringSubmatch(line)

		min := utils.MustInt(matches[1])
		max := utils.MustInt(matches[2])
		character := matches[3]
		password := matches[4]
		x := string(password[min-1]) == character
		y := string(password[max-1]) == character

		if (x || y) && !(x && y) {
			validPasswordCount++
		}
	}

	return validPasswordCount
}
