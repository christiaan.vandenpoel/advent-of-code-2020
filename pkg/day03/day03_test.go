package day03_test

import (
	"testing"

	"advent-of-code-2020/pkg/day03"
)

var testPattern = `..##.......
#...#...#..
.#....#..#.
..#.#...#.#
.#...##..#.
..#.##.....
.#.#.#....#
.#........#
#.##...#...
#...##....#
.#..#...#.#`

func TestTobogganTrajectorySingleSlope(t *testing.T) {

	tests := []struct {
		in  string
		out int
	}{
		{testPattern, 7},
	}

	for _, test := range tests {
		actual := day03.TobogganTrajectorySingleSlope(test.in)
		if actual != test.out {
			t.Errorf("TobogganTrajectorySingleSlope(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}

func TestTobogganTrajectoryMultipleSlopes(t *testing.T) {
	tests := []struct {
		in  string
		out int
	}{
		{testPattern, 336},
	}

	for _, test := range tests {
		actual := day03.TobogganTrajectoryMultipleSlopes(test.in)
		if actual != test.out {
			t.Errorf("TobogganTrajectoryMultipleSlopes(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}
