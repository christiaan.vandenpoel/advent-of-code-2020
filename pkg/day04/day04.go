package day04

import (
	"advent-of-code-2020/pkg/utils"
	"regexp"
	"strings"
)

func CountValidPassports(input string) int {
	passports := strings.Split(input, "\n\n")
	count := 0

	for _, passport := range passports {
		fields := strings.FieldsFunc(passport, splitter)

		fieldsMap := make(map[string]string)

		for _, field := range fields {
			parts := strings.Split(field, ":")
			fieldsMap[parts[0]] = parts[1]
		}
		if correctAmountOfFields(fieldsMap) {
			count++
		}
	}
	return count
}

func CountValidPassportsPartTwo(input string) int {
	passports := strings.Split(input, "\n\n")
	count := 0

	for _, passport := range passports {
		fields := strings.FieldsFunc(passport, splitter)

		fieldsMap := make(map[string]string)

		for _, field := range fields {
			parts := strings.Split(field, ":")
			fieldsMap[parts[0]] = parts[1]
		}
		if correctAmountOfFields(fieldsMap) &&
			validBirthYear(fieldsMap) &&
			validIssueYear(fieldsMap) &&
			validExpirationYear(fieldsMap) &&
			validHeight(fieldsMap) &&
			validHairColor(fieldsMap) &&
			validEyeColor(fieldsMap) &&
			validPassportId(fieldsMap) {
			count++
		}
	}
	return count
}

func correctAmountOfFields(fieldsMap map[string]string) bool {
	if len(fieldsMap) == 8 {
		return true
	}
	if len(fieldsMap) == 7 {
		if _, ok := fieldsMap["cid"]; !ok {
			return true
		}
	}
	return false
}

func validBirthYear(fieldsMap map[string]string) bool {
	if _, ok := fieldsMap["byr"]; !ok {
		return false
	}
	year := utils.MustInt(fieldsMap["byr"])
	if year >= 1920 && year <= 2002 {
		return true
	}
	return false
}

func validIssueYear(fieldsMap map[string]string) bool {
	if _, ok := fieldsMap["iyr"]; !ok {
		return false
	}
	year := utils.MustInt(fieldsMap["iyr"])
	if year >= 2010 && year <= 2020 {
		return true
	}
	return false
}

func validExpirationYear(fieldsMap map[string]string) bool {
	if _, ok := fieldsMap["eyr"]; !ok {
		return false
	}

	year := utils.MustInt(fieldsMap["eyr"])
	if year >= 2020 && year <= 2030 {
		return true
	}
	return false
}

func validHeight(fieldsMap map[string]string) bool {
	if _, ok := fieldsMap["hgt"]; !ok {
		return false
	}
	hgt := fieldsMap["hgt"]
	if strings.HasSuffix(hgt, "cm") {
		hgtCm := utils.MustInt(strings.ReplaceAll(hgt, "cm", ""))
		if hgtCm >= 150 && hgtCm <= 193 {
			return true
		}
		return false
	}
	if strings.HasSuffix(hgt, "in") {
		hgtInch := utils.MustInt(strings.ReplaceAll(hgt, "in", ""))
		if hgtInch >= 59 && hgtInch <= 76 {
			return true
		}
		return false
	}
	return false
}

func validHairColor(fieldsMap map[string]string) bool {
	if _, ok := fieldsMap["hcl"]; !ok {
		return false
	}

	regex := regexp.MustCompile("^#[0-9a-f]{6}$")
	if regex.MatchString(fieldsMap["hcl"]) {
		return true
	}
	return false
}

func validEyeColor(fieldsMap map[string]string) bool {
	if _, ok := fieldsMap["ecl"]; !ok {
		return false
	}
	regex := regexp.MustCompile("^amb|blu|brn|gry|grn|hzl|oth$")
	if regex.MatchString(fieldsMap["ecl"]) {
		return true
	}
	return false
}

func validPassportId(fieldsMap map[string]string) bool {
	if _, ok := fieldsMap["pid"]; !ok {
		return false
	}
	regex := regexp.MustCompile("^[0-9]{9}$")
	if regex.MatchString(fieldsMap["pid"]) {
		return true
	}
	return false
}

func splitter(r rune) bool {
	return r == ' ' || r == '\n'
}
