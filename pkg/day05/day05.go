package day05

import (
	"sort"
	"strings"
)

func BinaryBoardingHighestSeatId(boardingPassesList string) int {
	var boardingPasses []*BoardingPass

	for _, boardingpassString := range strings.Split(boardingPassesList, "\n") {
		boardingPasses = append(boardingPasses, NewBoardingPass(boardingpassString))
	}

	sort.Slice(boardingPasses, func(i, j int) bool {
		return boardingPasses[i].SeatId() > boardingPasses[j].SeatId()
	})

	return boardingPasses[0].SeatId()
}

func BinaryBoardingFindMySeatId(boardingPassesList string) int {
	var boardingPasses []*BoardingPass

	for _, boardingpassString := range strings.Split(boardingPassesList, "\n") {
		boardingPasses = append(boardingPasses, NewBoardingPass(boardingpassString))
	}

	sort.Slice(boardingPasses, func(i, j int) bool {
		return boardingPasses[i].SeatId() < boardingPasses[j].SeatId()
	})

	for idx, boardingPass := range boardingPasses {
		if idx == 0 || idx == len(boardingPasses)-1 {
			continue
		}
		previous := boardingPasses[idx-1]
		if previous.SeatId()+2 == boardingPass.SeatId() {
			return boardingPass.SeatId() - 1
		}
	}

	return 0
}

type BoardingPass struct {
	row    int
	column int
	seatId int
}

func NewBoardingPass(input string) *BoardingPass {
	result := &BoardingPass{row: 127}

	rowUpper := 127
	rowLower := 0
	for _, step := range input[0:7] {
		rowLower, rowUpper = TakeStep(step, rowLower, rowUpper)
	}
	result.row = rowLower

	columnUpper := 7
	columnLower := 0
	for _, step := range input[7:10] {
		columnLower, columnUpper = TakeStep(step, columnLower, columnUpper)
	}
	result.column = columnUpper
	result.seatId = result.column + result.row*8
	return result
}

func (boardingPass *BoardingPass) Row() int {
	return boardingPass.row
}

func (boardingPass *BoardingPass) Column() int {
	return boardingPass.column
}
func (boardingPass *BoardingPass) SeatId() int {
	return boardingPass.seatId
}

func TakeStep(step rune, lower int, upper int) (int, int) {
	if step == 'F' || step == 'L' {
		return lower, (upper-lower)/2 + lower
	} else if step == 'B' || step == 'R' {
		return (upper-lower)/2 + lower + 1, upper
	}
	return 0, 0
}
