package day05_test

import (
	"testing"

	"advent-of-code-2020/pkg/day05"
)

func TestBinaryBoardingHighestSeatId(t *testing.T) {
	tests := []struct {
		in  string
		out int
	}{
		{`FBFBBFFRLR
BFFFBBFRRR
FFFBBBFRRR
BBFFBBFRLL`, 820},
	}

	for _, test := range tests {
		actual := day05.BinaryBoardingHighestSeatId(test.in)
		if actual != test.out {
			t.Errorf("BinaryBoardingHighestSeatId(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}

func TestBinaryBoardingProcessor(t *testing.T) {
	tests := []struct {
		in     string
		row    int
		column int
		seatId int
	}{
		{`FFFFFFFLLL`, 0, 0, 0},
		{`FFFFFFBLLL`, 1, 0, 8},
		{`BBBBBBBLLL`, 127, 0, 127 * 8},
		{`FBFBBFFRLR`, 44, 5, 357},
		{`BFFFBBFRRR`, 70, 7, 567},
		{`FFFBBBFRRR`, 14, 7, 119},
		{`BBFFBBFRLL`, 102, 4, 820},
	}

	for _, test := range tests {
		actual := day05.NewBoardingPass(test.in)
		if actual.Row() != test.row {
			t.Errorf("NewBoardingPass(%q).Row() => %d, want %d", test.in, actual.Row(), test.row)
		}
		if actual.Column() != test.column {
			t.Errorf("NewBoardingPass(%q).Column() => %d, want %d", test.in, actual.Column(), test.column)
		}
		if actual.SeatId() != test.seatId {
			t.Errorf("NewBoardingPass(%q).SeatId() => %d, want %d", test.in, actual.SeatId(), test.seatId)
		}

	}
}

func TestHalveStepper(t *testing.T) {
	tests := []struct {
		step          rune
		lower         int
		upper         int
		lowerExpected int
		upperExpected int
	}{
		{'F', 0, 127, 0, 63},
		{'B', 0, 127, 64, 127},
		{'B', 0, 63, 32, 63},
		{'F', 32, 63, 32, 47},
		{'B', 32, 47, 40, 47},
		{'B', 40, 47, 44, 47},
		{'F', 44, 47, 44, 45},
		{'F', 44, 45, 44, 44},
		{'R', 0, 7, 4, 7},
		{'L', 0, 7, 0, 3},
	}

	for _, test := range tests {
		actualLower, actualUpper := day05.TakeStep(test.step, test.lower, test.upper)
		if actualLower != test.lowerExpected || actualUpper != test.upperExpected {
			t.Errorf("TakeStep(%q, %d, %d) => (%d, %d), want (%d, %d)", test.step, test.lower, test.upper, actualLower, actualUpper, test.lowerExpected, test.upperExpected)
		}
	}

}
