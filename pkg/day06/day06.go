package day06

import (
	"strings"
)

func CustomCustomsAnyAnswerTotal(input string) int {
	groups := strings.Split(input, "\n\n")

	total := 0

	for _, group := range groups {
		set := make(map[rune]bool)

		people := strings.Split(group, "\n")
		for _, person := range people {
			for _, answer := range person {
				set[answer] = true
			}
		}
		total += len(set)
	}
	return total
}

func CustomCustomsEveryoneAnsweredTotal(input string) int {
	groups := strings.Split(input, "\n\n")

	total := 0

	for _, group := range groups {
		set := make(map[rune]int)

		people := strings.Split(group, "\n")
		for _, person := range people {
			for _, answer := range person {
				set[answer]++
			}
		}
		for _, v := range set {
			if v == len(people) {
				total++
			}
		}
	}
	return total
}
