package day06_test

import (
	"testing"

	"advent-of-code-2020/pkg/day06"
)

func TestCustomCustomsAnyAnswerTotal(t *testing.T) {
	tests := []struct {
		in  string
		out int
	}{
		{`abc

a
b
c

ab
ac

a
a
a
a

b
`, 11},
	}

	for _, test := range tests {
		actual := day06.CustomCustomsAnyAnswerTotal(test.in)
		if actual != test.out {
			t.Errorf("CustomCustomsAnyAnswerTotal(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}

func TestCustomCustomsEveryoneAnsweredTotal(t *testing.T) {
	tests := []struct {
		in  string
		out int
	}{
		{`abc

a
b
c

ab
ac

a
a
a
a

b`, 6},
	}

	for _, test := range tests {
		actual := day06.CustomCustomsEveryoneAnsweredTotal(test.in)
		if actual != test.out {
			t.Errorf("CustomCustomsEveryoneAnsweredTotal(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}
