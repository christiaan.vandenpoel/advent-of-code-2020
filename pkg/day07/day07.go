package day07

import (
	"advent-of-code-2020/pkg/utils"
	"strings"
)

func HandyHaversacksBagsForShinyGold(bagList string) int {

	bags := strings.Split(bagList, "\n")

	bagMap := make(map[string][]string)

	for _, bagDefinition := range bags {
		definition := strings.Split(bagDefinition, " contain ")

		bagName := strings.ReplaceAll(definition[0], " bags", "")
		if definition[1] == "no other bags." {
			continue
		}
		contents := strings.Split(strings.ReplaceAll(definition[1], ".", ""), ", ")

		for _, content := range contents {
			parts := strings.SplitN(strings.ReplaceAll(strings.ReplaceAll(content, " bags", ""), " bag", ""), " ", 2)
			bagMap[bagName] = append(bagMap[bagName], parts[1])
		}
	}

	set := make(map[string]bool)

	for bagName := range bagMap {
		findShinyGoldRecursive(set, bagMap, bagName, bagName)
	}

	return len(set)
}

func findShinyGoldRecursive(set map[string]bool, bagMap map[string][]string, bagName string, itemName string) {
	for _, content := range bagMap[itemName] {
		if content == "shiny gold" {
			set[bagName] = true
		} else {
			findShinyGoldRecursive(set, bagMap, bagName, content)
		}
	}
}

type bagContent struct {
	bagName string
	amount  int
}

func HandyHaversacksCountBagsInShinyGold(bagList string) int {
	bags := strings.Split(bagList, "\n")

	bagMap := make(map[string][]bagContent)

	for _, bagDefinition := range bags {
		definition := strings.Split(bagDefinition, " contain ")

		bagName := strings.ReplaceAll(definition[0], " bags", "")
		if definition[1] == "no other bags." {
			continue
		}
		contents := strings.Split(strings.ReplaceAll(definition[1], ".", ""), ", ")

		for _, content := range contents {
			parts := strings.SplitN(strings.ReplaceAll(strings.ReplaceAll(content, " bags", ""), " bag", ""), " ", 2)
			bagMap[bagName] = append(bagMap[bagName], bagContent{parts[1], utils.MustInt(parts[0])})
		}
	}

	// fmt.Printf("%#v\n", bagMap)

	total := countBags("shiny gold", bagMap)

	return total
}

func countBags(bagName string, bagMap map[string][]bagContent) int {
	total := 0

	for _, content := range bagMap[bagName] {
		total += content.amount

		total += content.amount * countBags(content.bagName, bagMap)
	}

	return total
}
