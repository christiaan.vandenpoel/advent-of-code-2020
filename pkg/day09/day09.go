package day09

import (
	"advent-of-code-2020/pkg/utils"
	"sort"
	"strings"
)

func EncodingErrorFirstInvalidWithPreamble(input string, preamble int) int {

	var xmas []int

	for _, line := range strings.Split(input, "\n") {
		xmas = append(xmas, utils.MustInt(line))
	}

	for idx, item := range xmas[preamble:] {
		found := false
		for firstIdx, firstItem := range xmas[idx : idx+preamble] {
			for _, secondItem := range xmas[idx+firstIdx+1 : idx+preamble] {
				if firstItem+secondItem == item {
					found = true
				}
			}
		}
		if !found {
			return item
		}
	}

	return 0
}

func EncodingErrorEncryptionWeakness(input string, preamble int) int {
	invalid := EncodingErrorFirstInvalidWithPreamble(input, preamble)

	var xmas []int

	for _, line := range strings.Split(input, "\n") {
		xmas = append(xmas, utils.MustInt(line))
	}

	for idx, _ := range xmas {
		total := xmas[idx]
		counter := 0

		for total < invalid {
			counter++
			total += xmas[idx+counter]
		}

		if total == invalid {
			sort.Ints(xmas[idx : idx+counter+1])
			return xmas[idx] + xmas[idx+counter]
		}
	}

	return 0
}
