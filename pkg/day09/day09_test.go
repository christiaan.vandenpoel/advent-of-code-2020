package day09_test

import (
	"testing"

	"advent-of-code-2020/pkg/day09"
)

func TestX(t *testing.T) {
	tests := []struct {
		in       string
		preamble int
		out      int
	}{
		{`35
20
15
25
47
40
62
55
65
95
102
117
150
182
127
219
299
277
309
576`, 5, 127},
	}

	for _, test := range tests {
		actual := day09.EncodingErrorFirstInvalidWithPreamble(test.in, test.preamble)
		if actual != test.out {
			t.Errorf("X(%q, %d) => %d, want %d", test.in, test.preamble, actual, test.out)
		}
	}
}

func TestY(t *testing.T) {
	tests := []struct {
		in       string
		preamble int
		out      int
	}{
		{`35
20
15
25
47
40
62
55
65
95
102
117
150
182
127
219
299
277
309
576`, 5, 15 + 47},
	}

	for _, test := range tests {
		actual := day09.EncodingErrorEncryptionWeakness(test.in, test.preamble)
		if actual != test.out {
			t.Errorf("EncodingErrorEncryptionWeakness(%q, %d) => %d, want %d", test.in, test.preamble, actual, test.out)
		}
	}
}
