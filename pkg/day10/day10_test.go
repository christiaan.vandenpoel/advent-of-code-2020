package day10_test

import (
	"testing"

	"advent-of-code-2020/pkg/day10"
)

var shortExample = `16
10
15
5
1
11
7
19
6
12
4`

var longExample = `28
33
18
42
31
14
46
20
48
47
24
23
49
45
19
38
39
11
1
32
25
35
8
17
7
9
4
2
34
10
3`

func TestAdapterArrayJoltDifference(t *testing.T) {
	tests := []struct {
		in  string
		out int
	}{
		{shortExample, 7 * 5},
		{longExample, 22 * 10},
	}

	for _, test := range tests {
		actual := day10.AdapterArrayJoltDifference(test.in)
		if actual != test.out {
			t.Errorf("AdapterArrayJoltDifference(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}

func TestAdapterArrayPossibleCombinations(t *testing.T) {
	tests := []struct {
		in  string
		out int
	}{
		{shortExample, 8},
		{longExample, 19208},
	}

	for _, test := range tests {
		actual := day10.AdapterArrayPossibleCombinations(test.in)
		if actual != test.out {
			t.Errorf("AdapterArrayPossibleCombinations(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}
