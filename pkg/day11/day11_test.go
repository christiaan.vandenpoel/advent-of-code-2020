package day11_test

import (
	"testing"

	"advent-of-code-2020/pkg/day11"
)

func TestSeatingSystemTotalOccupiedSeatsDirectlyAdjacent(t *testing.T) {
	tests := []struct {
		in  string
		out int
	}{
		{start, 37},
	}

	for _, test := range tests {
		actual := day11.SeatingSystemTotalOccupiedSeatsDirectlyAdjacent(test.in)
		if actual != test.out {
			t.Errorf("SeatingSystemTotalOccupiedSeatsDirectlyAdjacent(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}

func TestSeatingSystemTotalOccupiedSeatsVisiblyAdjacent(t *testing.T) {
	tests := []struct {
		in  string
		out int
	}{
		{start, 26},
	}

	for _, test := range tests {
		actual := day11.SeatingSystemTotalOccupiedSeatsVisiblyAdjacent(test.in)
		if actual != test.out {
			t.Errorf("SeatingSystemTotalOccupiedSeatsVisiblyAdjacent(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}

var start = `L.LL.LL.LL
LLLLLLL.LL
L.L.L..L..
LLLL.LL.LL
L.LL.LL.LL
L.LLLLL.LL
..L.L.....
LLLLLLLLLL
L.LLLLLL.L
L.LLLLL.LL`
