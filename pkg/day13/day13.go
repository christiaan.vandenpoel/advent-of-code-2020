package day13

import (
	"advent-of-code-2020/pkg/utils"
	"strings"
)

func ShuttleSearchFirstAvailableBus(input string) int {
	var earliestDepartureTime int
	var busIDs []int
	var effectiveBusID int
	var effectiveDepartureTime int

	for idx, line := range strings.Split(input, "\n") {
		if idx == 0 {
			earliestDepartureTime = utils.MustInt(line)
		}
		if idx == 1 {
			for _, id := range strings.Split(line, ",") {
				if id == "x" {
					continue
				}
				busIDs = append(busIDs, utils.MustInt(id))
			}
		}
	}

	for _, busID := range busIDs {
		nextDeparture := ((earliestDepartureTime / busID) + 1) * busID

		if effectiveDepartureTime == 0 || nextDeparture < effectiveDepartureTime {
			effectiveDepartureTime = nextDeparture
			effectiveBusID = busID
		}
	}

	return (effectiveDepartureTime - earliestDepartureTime) * effectiveBusID
}

func ShuttleSearchFirstTimestampWithSameDepartureDifferences(input string) int {
	// shameless plug from https://github.com/alexchao26/advent-of-code-go/blob/main/2020/day13/main.go#L45
	// can't get my head around the actual math
	busLine := strings.Split(input, "\n")[1]

	var busses [][2]int

	for index, busID := range strings.Split(busLine, ",") {
		if busID != "x" {
			busses = append(busses, [2]int{index, utils.MustInt(busID)})
		}
	}

	var timeValue int
	runningProduct := 1
	for _, bus := range busses {
		index, busID := bus[0], bus[1]
		// this for loop adjusts the time until the constaint for this bus is met
		// i.e. ensure (time + index) is divisible by the busID to ensure the bus arrives
		for (timeValue+index)%busID != 0 {
			// running product is used to increment because it will not affect
			// the modulo of any of the previously scheduled busses, we've found
			// the frequency to match them.
			// e.g. if busID: 5 & index: 2, min timeValue is 3 b/c (3+2)%5 == 0
			//      if the running product were 5, adding 5 means (8+2)%5 == 0
			//      and (3 + 5x + 2) % 5 == 0 for any x
			timeValue += runningProduct
		}
		runningProduct *= busID
	}

	return timeValue
}
