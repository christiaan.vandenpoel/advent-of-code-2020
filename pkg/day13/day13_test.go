package day13_test

import (
	"testing"

	"advent-of-code-2020/pkg/day13"
)

func TestX(t *testing.T) {
	tests := []struct {
		in  string
		out int
	}{
		{`939
7,13,x,x,59,x,31,19`, 295},
	}

	for _, test := range tests {
		actual := day13.ShuttleSearchFirstAvailableBus(test.in)
		if actual != test.out {
			t.Errorf("ShuttleSearchFirstAvailableBus(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}

func TestShuttleSearchFirstTimestampWithSameDepartureDifferences(t *testing.T) {
	tests := []struct {
		in  string
		out int
	}{
		{`bla
7,13,x,x,59,x,31,19`, 1068781},
		{`bla
17,x,13,19`, 3417},
		{`bla
67,7,59,61`, 754018},
		{`bla
67,x,7,59,61`, 779210},
		{`bla
67,7,x,59,61`, 1261476},
		{`bla
1789,37,47,1889`, 1202161486},
	}

	for _, test := range tests {
		actual := day13.ShuttleSearchFirstTimestampWithSameDepartureDifferences(test.in)
		if actual != test.out {
			t.Errorf("ShuttleSearchFirstTimestampWithSameDepartureDifferences(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}
