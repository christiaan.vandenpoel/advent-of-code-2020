package day14_test

import (
	"reflect"
	"testing"

	"advent-of-code-2020/pkg/day14"
)

func TestDockingDataMemorySum(t *testing.T) {
	tests := []struct {
		in  string
		out int
	}{
		{`mask = XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X
mem[8] = 11
mem[7] = 101
mem[8] = 0`, 165},
	}

	for _, test := range tests {
		actual := day14.DockingDataMemorySum(test.in)
		if actual != test.out {
			t.Errorf("X(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}

func TestMasks(t *testing.T) {
	tests := []struct {
		mask string
		or   int
		and  int
	}{
		{"XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX", 0, 0xFFFFFFFFF},
		{"XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X", 0 | 0x40, 0xFFFFFFFFF & 0xFFFFFFFFD},
	}

	for _, test := range tests {
		actualOr, actualAnd := day14.MaskToBitmaps(test.mask)
		if actualAnd != test.and || actualOr != test.or {
			t.Errorf("MaskToBitmaps(%q) => (0x%x, 0x%x), want (0x%x, 0x%x)", test.mask, actualOr, actualAnd, test.or, test.and)
		}
	}
}

func TestDockingDataMemorySumVersion2(t *testing.T) {
	tests := []struct {
		in  string
		out int
	}{
		{`mask = mask = 000000000000000000000000000000X1001X
mem[42] = 100
mask = 00000000000000000000000000000000X0XX
mem[26] = 1`, 208},
	}

	for _, test := range tests {
		actual := day14.DockingDataMemorySumVersion2(test.in)
		if actual != test.out {
			t.Errorf("DockingDataMemorySumVersion2(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}

func TestMasksVersion2(t *testing.T) {
	tests := []struct {
		mask      string
		address   int
		addresses []int
	}{
		{"000000000000000000000000000000X1001X", 42, []int{26, 27, 58, 59}},
		{"00000000000000000000000000000000X0XX", 26, []int{16, 17, 18, 19, 24, 25, 26, 27}},
	}

	for _, test := range tests {
		mask := day14.NewMask(test.mask)
		actual := day14.MemoryAddresses(mask, test.address)
		if !reflect.DeepEqual(actual, test.addresses) {
			t.Errorf("MemoryAddresses(%q, %d) => (%v), want (%v)", test.mask, test.address, actual, test.addresses)
		}
	}
}
