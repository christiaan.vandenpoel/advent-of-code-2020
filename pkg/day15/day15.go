package day15

import (
	"advent-of-code-2020/pkg/utils"
	"strings"
)

func RambunctiousRecitationNthNumber(input string, count int) int {
	// TODO: optimize for count == 30_000_000
	var inputList []int

	for _, i := range strings.Split(input, ",") {
		inputList = append(inputList, utils.MustInt(i))
	}

	lastSpoken := -1
	spokenWhen := make(map[int][]int)
	everySpoken := make(map[int]int)

	for cnt := 0; cnt < count; cnt++ {
		if lastSpoken != -1 {
			everySpoken[lastSpoken]++

			if _, ok := spokenWhen[lastSpoken]; !ok {
				spokenWhen[lastSpoken] = []int{}
			}
			spokenWhen[lastSpoken] = append(spokenWhen[lastSpoken], cnt-1)
		}

		if cnt < len(inputList) {
			lastSpoken = inputList[cnt]
		} else {
			if everySpoken[lastSpoken] == 1 {
				lastSpoken = 0
			} else {
				l := len(spokenWhen[lastSpoken])
				last := spokenWhen[lastSpoken][l-1]
				nextToLast := spokenWhen[lastSpoken][l-2]

				lastSpoken = last - nextToLast
			}
		}

	}

	return lastSpoken
}
