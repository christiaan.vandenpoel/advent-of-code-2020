package day16

import (
	"advent-of-code-2020/pkg/utils"
	"strings"
)

func TicketTranslationInvalidTickets(input string) int {
	ticketInfo := ParseInput(input)

	sum := 0

	for _, nearbyTicket := range ticketInfo.NearbyTickets {
		for _, nr := range nearbyTicket {
			appliesAtLeastToOneFilter := false
			for _, filter := range ticketInfo.Filters {
				for _, minMax := range filter {
					if nr >= minMax.Min && nr <= minMax.Max {
						appliesAtLeastToOneFilter = true
					}
				}
			}
			if !appliesAtLeastToOneFilter {
				sum += nr
			}
		}
	}

	return sum
}

type Range struct {
	Min, Max int
}

type TicketInfo struct {
	Filters       map[string][]Range
	YourTicket    []int
	NearbyTickets [][]int
}

func ParseInput(input string) (result TicketInfo) {
	result = TicketInfo{
		Filters:       make(map[string][]Range),
		YourTicket:    make([]int, 0),
		NearbyTickets: make([][]int, 0),
	}

	parts := strings.Split(input, "\n\n")

	// filters
	for _, line := range strings.Split(parts[0], "\n") {
		// e.g. "class: 1-3 or 5-7"
		p := strings.Split(line, ": ")
		filterName := p[0]
		result.Filters[filterName] = make([]Range, 0)
		for _, ranges := range strings.Split(p[1], " or ") {
			minMax := strings.Split(ranges, "-")
			result.Filters[filterName] = append(result.Filters[filterName], Range{Min: utils.MustInt(minMax[0]), Max: utils.MustInt(minMax[1])})
		}
	}

	// your ticket
	for _, nr := range strings.Split(strings.Split(parts[1], "\n")[1], ",") {
		result.YourTicket = append(result.YourTicket, utils.MustInt(nr))
	}

	// nearby tickets
	nbTickets := strings.Split(parts[2], "\n")

	for idx, nbTicket := range nbTickets {
		if idx == 0 {
			continue
		}
		result.NearbyTickets = append(result.NearbyTickets, []int{})

		for _, nr := range strings.Split(nbTicket, ",") {
			result.NearbyTickets[idx-1] = append(result.NearbyTickets[idx-1], utils.MustInt(nr))
		}
	}

	return
}

func Y(_ string) int { return 0 }
