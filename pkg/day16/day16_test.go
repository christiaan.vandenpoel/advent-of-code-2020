package day16_test

import (
	"reflect"
	"testing"

	"advent-of-code-2020/pkg/day16"
)

func TestTicketTranslationInvalidTickets(t *testing.T) {
	tests := []struct {
		in  string
		out int
	}{
		{`class: 1-3 or 5-7
row: 6-11 or 33-44
seat: 13-40 or 45-50

your ticket:
7,1,14

nearby tickets:
7,3,47
40,4,50
55,2,20
38,6,12`, 71},
	}

	for _, test := range tests {
		actual := day16.TicketTranslationInvalidTickets(test.in)
		if actual != test.out {
			t.Errorf("TicketTranslationInvalidTickets(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}

func TestTicketParseInput(t *testing.T) {
	tests := []struct {
		in            string
		filters       map[string][]day16.Range
		yourTicket    []int
		nearbyTickets [][]int
	}{
		{`class: 1-3 or 5-7

your ticket:
7,1,14

nearby tickets:
7,3,47`,
			map[string][]day16.Range{
				"class": {
					day16.Range{1, 3},
					day16.Range{5, 7},
				},
			},
			[]int{7, 1, 14},
			[][]int{
				{7, 3, 47},
			},
		},
		{`class: 1-3 or 5-7
row: 6-11 or 33-44
seat: 13-40 or 45-50

your ticket:
7,1,14

nearby tickets:
7,3,47
40,4,50
55,2,20
38,6,12`, map[string][]day16.Range{
			"class": {
				day16.Range{1, 3},
				day16.Range{5, 7},
			},
			"row": {
				day16.Range{6, 11},
				day16.Range{33, 44},
			},
			"seat": {
				day16.Range{13, 40},
				day16.Range{45, 50},
			},
		},
			[]int{7, 1, 14},
			[][]int{
				{7, 3, 47},
				{40, 4, 50},
				{55, 2, 20},
				{38, 6, 12},
			},
		},
	}

	for _, test := range tests {
		actual := day16.ParseInput(test.in)
		if !reflect.DeepEqual(actual.Filters, test.filters) {
			t.Errorf("ParseInput(%q).Filters => %v, want %v", test.in, actual.Filters, test.filters)
		}

		if !reflect.DeepEqual(actual.YourTicket, test.yourTicket) {
			t.Errorf("ParseInput(%q).YourTicket => %v, want %v", test.in, actual.YourTicket, test.yourTicket)
		}

		if !reflect.DeepEqual(actual.NearbyTickets, test.nearbyTickets) {
			t.Errorf("ParseInput(%q).NearbyTickets => %v, want %v", test.in, actual.NearbyTickets, test.nearbyTickets)
		}
	}
}

func TestY(t *testing.T) {
	tests := []struct {
		in  string
		out int
	}{
		{"", 0},
	}

	for _, test := range tests {
		actual := day16.Y(test.in)
		if actual != test.out {
			t.Errorf("Y(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}
