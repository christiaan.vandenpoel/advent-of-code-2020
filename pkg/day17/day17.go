package day17

import (
	"advent-of-code-2020/pkg/utils"
	"fmt"
	"strings"
)

func ConwayCubesAfter6Cycles(input string) int {

	fmt.Println(input)
	space := make(map[utils.Coordinate]bool)

	for y, line := range strings.Split(input, "\n") {
		for x, char := range line {
			if char == '#' {
				space[utils.Coordinate{x, y, 0}] = true
			}
		}
	}

	fmt.Printf("%#v\n", space)
	for cnt := 0; cnt < 6; cnt++ {
		newSpace := make(map[utils.Coordinate]bool)
		for coordinate, active := range space {
			if active {
				nbrOfActiveNeighbours := nbrOfActiveNeighbours(space, coordinate)
				if nbrOfActiveNeighbours == 2 || nbrOfActiveNeighbours == 3 {
					newSpace[coordinate] = true
				}
			} else {
				nbrOfActiveNeighbours := nbrOfActiveNeighbours(space, coordinate)
				if nbrOfActiveNeighbours == 3 {
					newSpace[coordinate] = true
				}
			}
		}
		space = newSpace
		fmt.Printf("%#v\n", newSpace)
		fmt.Printf("%#v\n", space)
	}

	return len(space)
}

func nbrOfActiveNeighbours(space map[utils.Coordinate]bool, coordinate utils.Coordinate) int {
	// TODO: finalize this
	result := 2

	return result
}

func Y(_ string) int { return 0 }
