package day17_test

import (
	"testing"

	"advent-of-code-2020/pkg/day17"
)

func TestConwayCubesAfter6Cycles(t *testing.T) {
	tests := []struct {
		in  string
		out int
	}{
		// 		{`.#.
		// ..#
		// ###`, 112},
	}

	for _, test := range tests {
		actual := day17.ConwayCubesAfter6Cycles(test.in)
		if actual != test.out {
			t.Errorf("ConwayCubesAfter6Cycles(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}

func TestY(t *testing.T) {
	tests := []struct {
		in  string
		out int
	}{
		{"", 0},
	}

	for _, test := range tests {
		actual := day17.Y(test.in)
		if actual != test.out {
			t.Errorf("Y(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}
