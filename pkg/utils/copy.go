package utils

func CopyIntSlice(input []int) []int {
	out := make([]int, len(input))
	copy(out, input)
	return out
}
