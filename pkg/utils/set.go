package utils

type Set struct {
	internal map[int]bool
}

func NewSet() *Set {
	return &Set{internal: make(map[int]bool)}
}

func (set *Set) Length() int {
	return len(set.internal)
}

func (set *Set) Add(value int) {
	set.internal[value] = true
}

func (set *Set) Contains(value int) bool {
	_, ok := set.internal[value]
	return ok
}
