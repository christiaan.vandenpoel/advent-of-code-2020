package utils

import "testing"

func TestSet(t *testing.T) {
	set := NewSet()

	actual := set.Length()
	if 0 != actual {
		t.Errorf("set.Length() = %d, expected %d", actual, 0)
	}

	set.Add(1)

	actual = set.Length()
	if 1 != actual {
		t.Errorf("set.Length() = %d, expected %d", actual, 1)
	}

	actualContains := set.Contains(1)
	if actualContains != true {
		t.Errorf("set.Contains(1) = %v, expected %v", actualContains, true)
	}

	actualContains = set.Contains(2)
	if actualContains != false {
		t.Errorf("set.Contains(2) = %v, expected %v", actualContains, false)
	}

	set.Add(1)

	actual = set.Length()
	if 1 != actual {
		t.Errorf("set.Length() = %d, expected %d", actual, 1)
	}

	set.Add(5)

	actual = set.Length()
	if 2 != actual {
		t.Errorf("set.Length() = %d, expected %d", actual, 2)
	}
}
